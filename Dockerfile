FROM ghcr.io/prefix-dev/pixi:0.19.1 AS build

COPY . /app
WORKDIR /app
RUN pixi run build-wheel
RUN pixi run --environment prod postinstall-production
RUN pixi shell-hook -e prod > /shell-hook
RUN echo "python -m trading_bot.src.lib_check" >> /shell-hook

FROM ubuntu:22.04 AS production

# only copy the production environment into prod container
COPY --from=build /app/.pixi/envs/prod /app/.pixi/envs/prod
COPY --from=build /shell-hook /shell-hook
WORKDIR /app
CMD ["/bin/bash", "/shell-hook"]