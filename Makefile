requirements:
	pip-chill > requirements.txt
lint:
	ruff check . --fix
init:
	pip install -r requirements.txt
	pre-commit install
